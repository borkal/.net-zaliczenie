﻿CREATE TABLE [dbo].[Movie]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Title] NCHAR(10) NULL, 
    [Genre] NCHAR(10) NULL, 
    [ReleaseDate] DATETIME NULL
)
