﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MovieSearch.Models;
using MovieSearch.MovieApi;

namespace MovieSearch.Controllers
{
    [Authorize(Roles = "User")]
    public class MovieController : Controller
    {
        private readonly MovieDbApi _movieApi;
        private ApplicationDbContext db = new ApplicationDbContext();

        public MovieController()
        {
            _movieApi = new MovieDbApi();
            PopulateGenres();
        }

        private Dictionary<int, string> _genreList { get; set; }

        // GET: Movie
        public ActionResult Index(string searchString, int? releaseDate, bool? includeAdultContent, int? genreId)
        {
            ViewBag.GenreList = _genreList;
            if (string.IsNullOrWhiteSpace(searchString) && !releaseDate.HasValue) return View(new List<MovieModel>());

            var searchMovieTask = _movieApi.SearchMovies(searchString, includeAdultContent != null && includeAdultContent.Value);
            if (searchMovieTask != null)
            {
                var toReturn = new List<MovieModel>();
                var searchedMovies = searchMovieTask.Result;

                var includeDate = releaseDate.HasValue;
                var includeGenre = genreId != 0;
                if (includeDate)
                {
                    var date = new DateTime(releaseDate.Value,1,1);
                    searchedMovies = searchMovieTask.Result.Where(x => x.release_date >= date).ToList();
                }

                if (includeGenre)
                {
                    searchedMovies = searchedMovies.Where(x => x.genre_ids.Contains((int)genreId)).ToList();
                }


                foreach (var result in searchedMovies)
                {
                    var movie = new MovieModel
                    {
                        MovieId = result.id,
                        Title = result.title,
                        ImgPath = "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + result.backdrop_path,
                        Overview = result.overview,
                        ReleaseDate = result.release_date,
                        VoteAverage = result.vote_average,
                        Genres = new List<string>()
                    };

                    foreach (var genre in result.genre_ids)
                        movie.Genres.Add(_genreList.FirstOrDefault(x => x.Key == genre).Value);

                    toReturn.Add(movie);
                }

                return View(toReturn);
            }

            return View(new List<MovieModel>());
        }

        public ActionResult MyFavouriteMovies()
        {
            return View();
        }

        private void PopulateGenres()
        {
            _genreList = new Dictionary<int, string>();
            _genreList.Add(0, "(None)");
            _genreList.Add(28, "Action");
            _genreList.Add(12, "Adventure");
            _genreList.Add(16, "Animation");
            _genreList.Add(35, "Comedy");
            _genreList.Add(80, "Crime");
            _genreList.Add(99, "Documentary");
            _genreList.Add(18, "Drama");
            _genreList.Add(10751, "Family");
            _genreList.Add(36, "History");
            _genreList.Add(27, "Horror");
            _genreList.Add(10402, "Music");
            _genreList.Add(9648, "Mystery");
            _genreList.Add(10749, "Romance");
            _genreList.Add(878, "Science Fiction");
            _genreList.Add(10770, "TV Movie");
            _genreList.Add(53, "Thriller");
            _genreList.Add(10752, "War");
            _genreList.Add(37, "Western");
        }
    }
}