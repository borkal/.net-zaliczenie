﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Management;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MovieSearch.Models;
using MovieSearch.MovieApi;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MovieSearch.Controllers
{
    public class MovieSearchApiController : ApiController
    {
        private ApplicationDbContext db { get; set; }

        public MovieSearchApiController()
        {
            db = new ApplicationDbContext();
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddMovieToFavourite/{movieId}")]
        public ActionResult AddMovieToFavourite(int movieId)
        {
            var currentUserId = User.Identity.GetUserId();
            if (db.UserMovies.Any(x => x.MovieId == movieId && x.UserId == currentUserId))
            {
                return new ContentResult{Content = "Movie has been already added" };
            }
            db.UserMovies.Add(new UserMovies
            {
                MovieId = movieId,
                UserId = currentUserId
            });
            db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("RemoveFavouriteMovie/{movieId}")]
        public ActionResult RemoveFavouriteMovie(int movieId)
        {
            var currentUserId = User.Identity.GetUserId();
            var objectToDelete = db.UserMovies.FirstOrDefault(x => x.MovieId == movieId && x.UserId == currentUserId);
            if (objectToDelete != null)
            {
                db.UserMovies.Attach(objectToDelete);
                db.UserMovies.Remove(objectToDelete);
                db.SaveChanges();
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("SelectFavouriteMovies")]
        public JsonResult SelectFavouriteMovies()
        {
            var userId = User.Identity.GetUserId();
            var favouriteMoviesIds = db.UserMovies.Where(x => x.UserId == userId).Select(x => x.MovieId).ToList();
            var result = new List<MovieModel>();

            foreach (var id in favouriteMoviesIds)
            {
                RestClient restClient = new RestClient("https://api.themoviedb.org/3/movie/" + id + "?api_key=9705c16c472a7f9dabe11b233f5e3a22");
                RestRequest restRequest = new RestRequest();
                restRequest.Method = Method.GET;
                restRequest.AddParameter("undefined", "{}", ParameterType.RequestBody);
                var response = restClient.Execute(restRequest);
                var joResponse = JObject.Parse(response.Content);
                var movie = joResponse.ToObject<Movie>();

                var genreList = new List<string>();
                foreach (var genre in movie.genres)
                {
                    var genreValue = genre.ElementAt(1).Value;
                    genreList.Add(genreValue);
                }

                var movieModel = new MovieModel()
                {
                    Title = movie.title,
                    MovieId = movie.id,
                    ReleaseDate = movie.release_date,
                    VoteAverage = movie.vote_average,
                    Genres = genreList,
                    ImgPath = "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + movie.backdrop_path,

                };
                result.Add(movieModel);
            }

            return new JsonResult{Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }
        
    }
}
