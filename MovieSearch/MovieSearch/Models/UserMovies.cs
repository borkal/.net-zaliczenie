﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieSearch.Models
{
    [Table("UserMovies")]
    public class UserMovies
    {
        [Key]
        public int IdRecord { get; set; }
        public int MovieId { get; set; }
        public string UserId { get; set; }
    }
}