﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.Models
{
    public class MovieModel
    {
        public MovieModel()
        {
            PopulateGenres();
        }
        public int MovieId { get; set; }
        public string Title { get; set; }
        public decimal VoteAverage { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string Overview { get; set; }
        public List<string> Genres { get; set; }
        public string ImgPath { get; set; }// = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
        public Dictionary<int,string> AllGenres = new Dictionary<int, string>();

        private void PopulateGenres()
        {
            AllGenres = new Dictionary<int, string>();
            AllGenres.Add(28, "Action");
            AllGenres.Add(12, "Adventure");
            AllGenres.Add(16, "Animation");
            AllGenres.Add(35, "Comedy");
            AllGenres.Add(80, "Crime");
            AllGenres.Add(99, "Documentary");
            AllGenres.Add(18, "Drama");
            AllGenres.Add(10751, "Family");
            AllGenres.Add(36, "History");
            AllGenres.Add(27, "Horror");
            AllGenres.Add(10402, "Music");
            AllGenres.Add(9648, "Mystery");
            AllGenres.Add(10749, "Romance");
            AllGenres.Add(878, "Science Fiction");
            AllGenres.Add(10770, "TV Movie");
            AllGenres.Add(53, "Thriller");
            AllGenres.Add(10752, "War");
            AllGenres.Add(37, "Western");
        }

    }
}