﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.MovieApi
{
    public class Movie
    {
        public int id { get; set; }
        public string title { get; set; }
        public decimal vote_average { get; set; }
        public DateTime? release_date{ get; set; }
        public string overview { get; set; }
        public List<int> genre_ids { get; set; }
        public List<Dictionary<string,string>> genres { get; set; }
        public string backdrop_path { get; set; }

    }
}