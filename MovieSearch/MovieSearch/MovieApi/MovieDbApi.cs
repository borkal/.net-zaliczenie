﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MovieSearch.MovieApi
{
    public class MovieDbApi
    {
        private const string URL = "https://api.themoviedb.org/3/search/movie?api_key=9705c16c472a7f9dabe11b233f5e3a22";
        public HttpClient HttpClient { get; set; }
        public MovieDbApi()
        {
            HttpClient = new HttpClient();
            

        }
        

        public async Task<List<Movie>> SearchMovies(string queryString, bool includeAdult = false)
        {
            //var url = URL + $"&query={queryString}&include_adult={includeAdult}";
            //RestClient restClient = new RestClient(URL);
            RestClient restClient = new RestClient("https://api.themoviedb.org/3/search/movie?query=" + queryString + $"&include_adult={includeAdult}" + "&api_key=9705c16c472a7f9dabe11b233f5e3a22");
            RestRequest restRequest = new RestRequest();
            restRequest.Method = Method.GET;
            restRequest.AddParameter("undefined", "{}", ParameterType.RequestBody);
            var response = restClient.Execute(restRequest);
            JObject joResponse = JObject.Parse(response.Content);
            var result = joResponse.ToObject<MovieApiResultClass>();
            return result.results;

        }
       
    }
}