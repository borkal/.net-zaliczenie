﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.MovieApi
{
    public class MovieApiResultClass
    {
        public int page { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }
        public List<Movie> results { get; set; }
    
}
}