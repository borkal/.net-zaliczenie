﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.MovieApi
{
    public class MovieGenre
    {
        public static Dictionary<int, string> _genreList { get; set; }

        public MovieGenre()
        {
            _genreList = new Dictionary<int, string>();
            _genreList.Add(0,"(None)");
            _genreList.Add(28, "Action");
            _genreList.Add(12, "Adventure");
            _genreList.Add(16, "Animation");
            _genreList.Add(35, "Comedy");
            _genreList.Add(80, "Crime");
            _genreList.Add(99, "Documentary");
            _genreList.Add(18, "Drama");
            _genreList.Add(10751, "Family");
            _genreList.Add(36, "History");
            _genreList.Add(27, "Horror");
            _genreList.Add(10402, "Music");
            _genreList.Add(9648, "Mystery");
            _genreList.Add(10749, "Romance");
            _genreList.Add(878, "Science Fiction");
            _genreList.Add(10770, "TV Movie");
            _genreList.Add(53, "Thriller");
            _genreList.Add(10752, "War");
            _genreList.Add(37, "Western");
        }

    }
}